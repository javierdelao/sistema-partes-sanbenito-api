package com.integracion.app.repository;

import com.integracion.app.model.Parte;
import org.springframework.stereotype.Repository;

@Repository
public class ParteDAO extends BeanDAO<Parte> {
}
