package com.integracion.app.repository;

import com.integracion.app.model.Infraccion;
import org.springframework.stereotype.Repository;

@Repository
public class InfraccionDAO extends BeanDAO<Infraccion> {
}
