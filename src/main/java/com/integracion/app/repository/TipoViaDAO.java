package com.integracion.app.repository;


import com.integracion.app.model.TipoVia;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

@Repository
public class TipoViaDAO extends BeanDAO<TipoVia> {

    @Transactional(readOnly = true)
    public TipoVia tipoVia(Long id, Boolean withFechCollention) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<TipoVia> criteriaQuery = criteriaBuilder.createQuery(TipoVia.class);
        Root<TipoVia> root = criteriaQuery.from(TipoVia.class);
        Path<Long> idPath = root.get("id");
        criteriaQuery.select(root)
                .where(criteriaBuilder.equal(idPath, id));
        Query<TipoVia> query = session.createQuery(criteriaQuery);
        TipoVia tipoVia = query.getSingleResult();
        if (tipoVia != null && withFechCollention.equals(true)) {
            tipoVia.fetchCollections();
        }
        return tipoVia;
    }


}
