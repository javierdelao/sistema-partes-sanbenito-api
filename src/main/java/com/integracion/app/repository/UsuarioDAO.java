package com.integracion.app.repository;

import com.integracion.app.model.Usuario;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

@Repository
public class UsuarioDAO extends BeanDAO<Usuario> {

    @Transactional(readOnly = true)
    public Usuario usuario(Usuario usuarioParam) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Usuario> criteriaQuery = criteriaBuilder.createQuery(Usuario.class);
        Root<Usuario> root = criteriaQuery.from(Usuario.class);
        Path<String> rutPath = root.get("rut");
        criteriaQuery.select(root)
                .where(criteriaBuilder.equal(rutPath, usuarioParam.getRut()));
        Query<Usuario> query = session.createQuery(criteriaQuery);
        try {
            Usuario Usuario = query.getSingleResult();
            return Usuario;
        }catch (Exception e){
            return null;
        }
    }

}
