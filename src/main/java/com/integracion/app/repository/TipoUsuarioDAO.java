package com.integracion.app.repository;

import com.integracion.app.model.TipoUsuario;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

@Repository
public class TipoUsuarioDAO extends BeanDAO<TipoUsuario> {

    @Transactional(readOnly = true)
    public TipoUsuario tipoUsuario(String glosa) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<TipoUsuario> criteriaQuery = criteriaBuilder.createQuery(TipoUsuario.class);
        Root<TipoUsuario> root = criteriaQuery.from(TipoUsuario.class);
        Path<String> glosaPath = root.get("glosa");
        criteriaQuery.select(root)
                .where(criteriaBuilder.equal(glosaPath, glosa));
        Query<TipoUsuario> query = session.createQuery(criteriaQuery);
        TipoUsuario tipoUsuario = query.getSingleResult();
        if (tipoUsuario != null) {
            tipoUsuario.setUsuarios(null);
        }
        return tipoUsuario;
    }
}
