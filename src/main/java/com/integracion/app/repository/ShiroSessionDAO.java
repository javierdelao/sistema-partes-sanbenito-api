/*
 * Copyright (c) 2017 Business News Americas Limitada - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.integracion.app.repository;


import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Juan Francisco Rodríguez
 * <p>
 * Shiro Sessions repository in case we need to app the sessions in database
 **/
@Repository
public class ShiroSessionDAO {

    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ShiroSessionDAO.class);

    /**
     * Hibernate session factory
     */
    protected SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    /**
     * Retrieves a session
     *
     * @param sessionId the session id
     * @return the session or null
     */
      /* @Transactional
    public org.apache.shiro.session.Session get(Serializable sessionId) {
     Session hibernateSession = sessionFactory.getCurrentSession();
        org.apache.shiro.session.Session session = null;
        String psql = "SELECT sessiondata " +
                "FROM shiro_session " +
                "WHERE id = ? ";
        NativeQuery query = hibernateSession.createNativeQuery(psql);
        query.setParameter(1, sessionId.toString());
        query.addScalar("sessiondata", StringType.INSTANCE);
        List<String> sessionStringList = query.list();
        if (!sessionStringList.isEmpty()) {
            session = deserialize(sessionStringList.get(0));
            if (session == null) {
                //invalid session
                delete(hibernateSession, session);
            }
        }

        return session;

        return null;
    }
    */


}
