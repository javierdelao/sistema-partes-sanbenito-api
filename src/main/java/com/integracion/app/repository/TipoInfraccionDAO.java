package com.integracion.app.repository;

import com.integracion.app.model.TipoInfraccion;
import org.springframework.stereotype.Repository;

@Repository
public class TipoInfraccionDAO extends BeanDAO<TipoInfraccion> {
}
