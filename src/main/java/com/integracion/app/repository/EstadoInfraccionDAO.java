package com.integracion.app.repository;

import com.integracion.app.model.EstadoInfraccion;
import com.integracion.app.model.TipoUsuario;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

@Repository
public class EstadoInfraccionDAO extends BeanDAO<EstadoInfraccion> {

    @Transactional(readOnly = true)
    public EstadoInfraccion estadoInfraccion(String glosa) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<EstadoInfraccion> criteriaQuery = criteriaBuilder.createQuery(EstadoInfraccion.class);
        Root<EstadoInfraccion> root = criteriaQuery.from(EstadoInfraccion.class);
        Path<String> glosaPath = root.get("glosa");
        criteriaQuery.select(root)
                .where(criteriaBuilder.equal(glosaPath, glosa));
        Query<EstadoInfraccion> query = session.createQuery(criteriaQuery);
        EstadoInfraccion estadoInfraccion = query.getSingleResult();

        return estadoInfraccion;
    }

}
