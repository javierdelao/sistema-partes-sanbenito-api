package com.integracion.app.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
public class EstadoInfraccion implements Serializable, Bean, LazyCollectorBean {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Basic
    @Column(length = 128, nullable = false)
    private String glosa;

    public EstadoInfraccion() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    @Override
    public void fetchCollections() {

    }
}
