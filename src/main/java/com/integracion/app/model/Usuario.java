package com.integracion.app.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(indexes = {},
        uniqueConstraints = {
                @UniqueConstraint(name = "rut_unique", columnNames = {"rut"})
        })
@JsonIgnoreProperties(ignoreUnknown = true)
public class Usuario implements Serializable, Bean, LazyCollectorBean{


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    @Column(length = 12, nullable = false)
    private String rut;

    @Basic
    @Column(length = 128, nullable = false)
    private String nombres;

    @Basic
    @Column(length = 128, nullable = false)
    private String aPaterno;

    @Basic
    @Column(length = 128, nullable = false)
    private String aMaterno;

    @Basic
    @Column(length = 128, nullable = false)
    private String password;

    @ManyToOne
    @JoinColumn(name="tipoUsuario_id", nullable=false)
    private TipoUsuario tipoUsuario;

    public Usuario() {
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getaPaterno() {
        return aPaterno;
    }

    public void setaPaterno(String aPaterno) {
        this.aPaterno = aPaterno;
    }

    public String getaMaterno() {
        return aMaterno;
    }

    public void setaMaterno(String aMaterno) {
        this.aMaterno = aMaterno;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public void fetchCollections() {

    }
}
