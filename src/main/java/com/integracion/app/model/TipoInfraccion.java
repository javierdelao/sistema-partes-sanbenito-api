package com.integracion.app.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
public class TipoInfraccion  implements Serializable, Bean, LazyCollectorBean {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Basic
    @Column(length = 128, nullable = false)
    private String glosa;

    @Basic
    @Column(length = 128, nullable = false)
    private String gravedad;

    public TipoInfraccion() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public String getGravedad() {
        return gravedad;
    }

    public void setGravedad(String gravedad) {
        this.gravedad = gravedad;
    }

    @Override
    public void fetchCollections() {

    }
}
