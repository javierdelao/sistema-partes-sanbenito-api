package com.integracion.app.model;



import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table
public class TipoVia implements Serializable, Bean, LazyCollectorBean {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Basic
    @Column(length = 128, nullable = false)
    private String glosa;

    @OneToMany(mappedBy="tipoVia")
    private List<Via> vias;

    public TipoVia() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public List<Via> getVias() {
        return vias;
    }

    public void setVias(List<Via> vias) {
        this.vias = vias;
    }

    @Override
    public void fetchCollections() {
        vias.size();
    }
}
