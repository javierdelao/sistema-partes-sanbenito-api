package com.integracion.app.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
public class Via implements Serializable, Bean, LazyCollectorBean {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Basic
    @Column(length = 128, nullable = false)
    private String numero;

    @Basic
    @Column(length = 128, nullable = false)
    private String calle;

    @ManyToOne
    @JoinColumn(name="tipoVia_id", nullable=false)
    private TipoVia tipoVia;

    public Via() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public TipoVia getTipoVia() {
        return tipoVia;
    }

    public void setTipoVia(TipoVia tipoVia) {
        this.tipoVia = tipoVia;
    }

    @Override
    public void fetchCollections() {

    }
}
