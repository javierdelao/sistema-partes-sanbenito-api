package com.integracion.app.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
public class Infraccion implements Serializable, Bean, LazyCollectorBean {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private Integer multaParcial;

    @ManyToOne
    @JoinColumn(name="tipoInfraccion_id", nullable=false)
    private TipoInfraccion tipoInfraccion;

    @ManyToOne
    @JoinColumn(name="estadoInfraccion_id", nullable=false)
    private EstadoInfraccion estadoInfraccion;

    @Basic
    @Column(length = 128, nullable = false)
    private String observacion;

    public Infraccion() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMultaParcial() {
        return multaParcial;
    }

    public void setMultaParcial(Integer multaParcial) {
        this.multaParcial = multaParcial;
    }

    public TipoInfraccion getTipoInfraccion() {
        return tipoInfraccion;
    }

    public void setTipoInfraccion(TipoInfraccion tipoInfraccion) {
        this.tipoInfraccion = tipoInfraccion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public EstadoInfraccion getEstadoInfraccion() {
        return estadoInfraccion;
    }

    public void setEstadoInfraccion(EstadoInfraccion estadoInfraccion) {
        this.estadoInfraccion = estadoInfraccion;
    }

    @Override
    public void fetchCollections() {

    }
}
