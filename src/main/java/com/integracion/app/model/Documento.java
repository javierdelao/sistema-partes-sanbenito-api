package com.integracion.app.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
public class Documento implements Serializable, Bean, LazyCollectorBean {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Basic
    @Column(length = 128, nullable = false)
    private String nombre;

    @Basic
    @Column(length = 128, nullable = false)
    private String ruta;

    public Documento() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    @Override
    public void fetchCollections() {

    }
}
