package com.integracion.app.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table
public class Parte implements Serializable, Bean, LazyCollectorBean {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Basic
    @Column(length = 12, nullable = false)
    private String rutInfractor;

    @Basic
    @Column(length = 12, nullable = false)
    private String rutDuenoVehiculo;

    @Basic
    private Integer multaTotal;

    @ManyToOne
    @JoinColumn(name="inspector_id", nullable=false)
    private Usuario inspector;

    @ManyToOne
    @JoinColumn(name="cajero_id", nullable=true)
    private Usuario cajero;

    @Basic
    @Column(length = 10, nullable = false)
    private String patente;

    @Basic
    private Boolean isEmpadronado;

    @Basic
    private Date fecha;

    public Parte() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRutInfractor() {
        return rutInfractor;
    }

    public void setRutInfractor(String rutInfractor) {
        this.rutInfractor = rutInfractor;
    }

    public String getRutDuenoVehiculo() {
        return rutDuenoVehiculo;
    }

    public void setRutDuenoVehiculo(String rutDuenoVehiculo) {
        this.rutDuenoVehiculo = rutDuenoVehiculo;
    }

    public Integer getMultaTotal() {
        return multaTotal;
    }

    public void setMultaTotal(Integer multaTotal) {
        this.multaTotal = multaTotal;
    }

    public Usuario getInspector() {
        return inspector;
    }

    public void setInspector(Usuario inspector) {
        this.inspector = inspector;
    }

    public Usuario getCajero() {
        return cajero;
    }

    public void setCajero(Usuario cajero) {
        this.cajero = cajero;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public Boolean getEmpadronado() {
        return isEmpadronado;
    }

    public void setEmpadronado(Boolean empadronado) {
        isEmpadronado = empadronado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public void fetchCollections() {

    }
}
