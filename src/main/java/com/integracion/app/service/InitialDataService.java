package com.integracion.app.service;

import com.integracion.app.model.*;
import com.integracion.app.repository.*;
import com.integracion.app.util.ConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;

@Service
public class InitialDataService {

    private ConfigProperties configProperties;

    private UsuarioDAO usuarioDAO;

    @Autowired
    public void setConfigProperties(ConfigProperties configProperties) {
        this.configProperties = configProperties;
    }

    @Autowired
    public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
        this.usuarioDAO = usuarioDAO;
    }

    @PostConstruct
    public void init() {

        if (configProperties.getMap().get("jdbc.auto").equals("update") || configProperties.getMap().get("jdbc.auto").equals("create")) {


        }

    }


}
