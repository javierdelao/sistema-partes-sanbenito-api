package com.integracion.app.controller.rest.controller;

import com.integracion.app.model.EstadoInfraccion;
import com.integracion.app.model.Infraccion;
import com.integracion.app.model.Usuario;
import com.integracion.app.repository.EstadoInfraccionDAO;
import com.integracion.app.repository.InfraccionDAO;
import com.integracion.app.repository.UsuarioDAO;
import com.integracion.app.response.InfraccionResponse;
import com.integracion.app.response.LoginResponse;
import com.integracion.app.response.PagoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ldap")
public class ldapController {

    private UsuarioDAO usuarioDAO;

    private EstadoInfraccionDAO estadoInfraccionDAO;

    private InfraccionDAO infraccionDAO;

    @Autowired
    public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
        this.usuarioDAO = usuarioDAO;
    }

    @Autowired
    public void setEstadoInfraccionDAO(EstadoInfraccionDAO estadoInfraccionDAO) {
        this.estadoInfraccionDAO = estadoInfraccionDAO;
    }
    @Autowired
    public void setInfraccionDAO(InfraccionDAO infraccionDAO) {
        this.infraccionDAO = infraccionDAO;
    }

    @RequestMapping(path = "/test", method = RequestMethod.GET)
    public Usuario test(){

        Usuario usuario=new Usuario();
        usuario.setNombres("test");
       return usuario;
    }

    @RequestMapping(path = "/login/{rut}/{password}", method = RequestMethod.GET)
    public LoginResponse login(@PathVariable String rut,
                               @PathVariable String password){
        LoginResponse loginResponse=new LoginResponse();
        Usuario usuario=new Usuario();
        usuario.setRut(rut);
        usuario.setPassword(password);
        usuario= usuarioDAO.usuario(usuario);
        loginResponse.setUsuario(usuario);
        if(usuario!=null){
            usuario.getTipoUsuario().setUsuarios(null);
            loginResponse.setStatus("success");
        }else{
            loginResponse.setStatus("invalid");
        }
        return loginResponse;
    }


    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public LoginResponse registrarUsuario(@RequestBody Usuario usuarioParam){
        LoginResponse loginResponse=new LoginResponse();
       try {
           usuarioDAO.insert(usuarioParam);
           loginResponse.setStatus("success");
       }catch (Exception e){
           usuarioParam=null;
           loginResponse.setStatus(e.getMessage());
       }
        loginResponse.setUsuario(usuarioParam);
        return loginResponse;
    }



    @RequestMapping(path = "/pago", method = RequestMethod.POST)
    public PagoResponse registrarPago(@RequestBody Infraccion infraccion){

        PagoResponse pagoResponse=new PagoResponse();
        try {
            infraccion.setEstadoInfraccion(estadoInfraccionDAO.estadoInfraccion("Pagada"));
            infraccionDAO.update(infraccion);
            pagoResponse.setStatus("success");
        }catch (Exception e){
            pagoResponse.setStatus(e.getMessage());
        }
        pagoResponse.setInfraccion(infraccion);
        return pagoResponse;
    }


    @RequestMapping(path = "/infraccion", method = RequestMethod.POST)
    public InfraccionResponse registrarInfraccion(@RequestBody Infraccion infraccion){
        InfraccionResponse response=new InfraccionResponse();
        try {
            infraccionDAO.insert(infraccion);
            response.setStatus("success");
        }catch (Exception e){
            response.setStatus(e.getMessage());
        }
        response.setInfraccion(infraccion);
        return response;
    }
}
