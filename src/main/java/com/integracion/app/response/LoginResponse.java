package com.integracion.app.response;

import com.integracion.app.model.Usuario;

public class LoginResponse {

    private Usuario usuario;

    private String status;

    public LoginResponse() {
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
