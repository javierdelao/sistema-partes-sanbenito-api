package com.integracion.app.response;

import com.integracion.app.model.Infraccion;

public class PagoResponse {

    private Infraccion infraccion;

    private String status;

    public PagoResponse() {
    }

    public Infraccion getInfraccion() {
        return infraccion;
    }

    public void setInfraccion(Infraccion infraccion) {
        this.infraccion = infraccion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
